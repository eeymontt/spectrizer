from PySide6.QtWidgets import (
    QPushButton,
    QVBoxLayout,
    QGroupBox,
    QComboBox
)

from PySide6.QtSerialPort import QSerialPort, QSerialPortInfo
from PySide6.QtCore import Qt, Signal

class DeviceBox(QGroupBox):

    BAUDRATE = QSerialPort.BaudRate.Baud115200

    requestNewConnection = Signal(str, int)
    terminateConnection = Signal()

    def __init__(self, parent=None):
        super().__init__("Device Settings", parent=parent)

        self.is_connected = False

        layout = QVBoxLayout()
        self.setLayout(layout)

        self.button_refresh = QPushButton('Refresh')
        self.combobox_ports = QComboBox()
        self.combobox_bauds = QComboBox()
        self.button_connect = QPushButton('Connect')

        # self.button_connect.setStyleSheet("border: 1px solid black; padding: 2px 5px; background: white")
        self.button_connect.setStatusTip('Start serial connection')

        layout.addWidget(self.button_refresh)
        layout.addWidget(self.combobox_ports)
        layout.addWidget(self.combobox_bauds)
        layout.addWidget(self.button_connect)
        layout.addStretch()

        self.cominfo = QSerialPortInfo()
        self.combobox_bauds.addItems(
            [str(baud) for baud in self.cominfo.standardBaudRates()]
        )
        index = self.combobox_bauds.findText(
            str(self.BAUDRATE),
            Qt.MatchFlag.MatchExactly
        )
        if index != -1:
            self.combobox_bauds.setItemData(
                index,
                "Default",
                Qt.ToolTipRole
            )
            self.combobox_bauds.setCurrentIndex(index)

        self.button_refresh.clicked.connect(self.refresh_ports)
        self.button_connect.clicked.connect(self.connect_to_device)

    def refresh_ports(self):
        '''Refresh ports selection with ports discovered on the system.'''
        ports = self.cominfo.availablePorts()
        # print("Name:", info.portName())
        # print("description:", info.description())
        # print("productIdentifier:", info.productIdentifier())
        # print("vendoridentifier:", info.vendorIdentifier())
        # print("manufacturer:", info.manufacturer())
        # print("serialNumber:", info.serialNumber())
        # print("systemLocation:", info.serialNumber())

        self.combobox_ports.clear()
        #self.combobox_ports.addItems(ports)
        for index, port in enumerate(ports):
            name = port.portName()
            self.combobox_ports.addItem(name)
            self.combobox_ports.setItemData(
                index,
                f"{port.description()} ({name})",
                Qt.ToolTipRole
            )

    def connect_to_device(self):
        if not self.is_connected:
            port = self.combobox_ports.currentText()
            baud = int(self.combobox_bauds.currentText())
            print("Requesting connection...")
            self.requestNewConnection.emit(port, baud)
            # assume it works!
            self.button_refresh.setEnabled(False)
            self.combobox_ports.setEnabled(False)
            self.combobox_bauds.setEnabled(False)
            self.button_connect.setStatusTip('Close serial connection')
            self.set_connection_status(True)
        else:
            print("Requesting close...")
            self.terminateConnection.emit()
            
            # assume it works!
            self.button_refresh.setEnabled(True)
            self.combobox_ports.setEnabled(True)
            self.combobox_bauds.setEnabled(True)
            self.button_connect.setStatusTip('Start serial connection')
            self.set_connection_status(False)
    
    def set_connection_status(self, isConnected: bool):
        self.is_connected = isConnected
        if self.is_connected:
            self.button_connect.setText("Disconnect")
        else:
            self.button_connect.setText("Connect")