from PySide6.QtCore import Slot, QMutex, QObject, QThread, QTimer, QWaitCondition, Signal, QThreadPool
from PySide6.QtWidgets import QApplication, QMessageBox, QTabWidget
import serial.tools.list_ports

from spectrizer.mainWindow import MainWindow
from spectrizer.serialReceiver import SerialReceiver
from spectrizer.packetParser import PacketParser#, Worker
from spectrizer.graphWidget import PlotWidget3D

import time
import numpy as np
from random import randint

class Controller:
    def __init__(self):
        self.app = QApplication([])
        self.main_window = MainWindow()

        self.app.aboutToQuit.connect(self.on_app_exit)

        ## Serial connections
        #  Currently using QSerialPort due to performance increase over PySerial
        self.serial = SerialReceiver()
        self.serial.readReady.connect(self.process_new_data)

        ## Internal receieve buffer and packet decoding
        self.parser = PacketParser()
        self.parser.newDataAvail.connect(self.update_plot_data)

        self.device_ui = self.main_window.device_box
        self.device_ui.refresh_ports() # Automatically populate on startup
        self.device_ui.requestNewConnection.connect(self.serial.open_port)
        self.device_ui.terminateConnection.connect(self.serial.close)
        #self.device_ui.set_connection_status()

        self.viewer_ui = self.main_window.glv
        self.viewer_ui.changeFullscreenStatus.connect(self.print_bah)

        # Add all plots to a central list
        self.plots: dict[str, PlotWidget3D]
        self.plots = {child.name: child for child in self.main_window.findChildren(PlotWidget3D)}
        # print(f"children: {self.plots}")

        self.create_plot_graphics_updater()

    def print_bah(self):
        print("Bahahaha")
    
    def update_plot_data(self, data):
        '''Provide new data to every registered plot.'''
        # self.print_data(data)
        for plt_name, plt in self.plots.items():
            if plt_name == 'Acceleration1':
                plt.updateData(
                    data[0],
                    y=(data[1],data[2],data[3])
                )
            elif plt_name == 'Acceleration2':
                plt.updateData(
                    data[0],
                    y=(data[4],data[5],data[6])
                )
            elif plt_name == 'Gyro':
                plt.updateData(
                    data[0],
                    y=(data[7],data[8],data[9])
                )
            elif plt_name == 'Mag':
                plt.updateData(
                    data[0],
                    y=(data[10],data[11],data[12])
                )
            elif plt_name == 'RPY':
                plt.updateData(
                    data[0],
                    y=(data[15],data[16],data[17])
                )
        # self.main_window.glv.viewer.setOrientation(data[15],data[16],data[17])

    def update_plot_visuals(self):
        '''Update the visuals for all relevant plots.'''

        # get visible tabs
        # visible_index = self.main_window.central_tab.currentIndex()
        # if visible_index == 0:
        #     self.plots[0].updateGraphics()
    
        # For now, just update them all!!!
        # for plt in self.plots:
        #     plt.updateGraphics()
        for plt in self.plots.values():
            plt.updateGraphics()
        
    def create_plot_graphics_updater(self):
        '''Create a means to update plot graphics.'''
        self.plt_gui_timer = QTimer()
        self.plt_gui_timer.setInterval(0)
        self.plt_gui_timer.timeout.connect(self.update_plot_visuals)
        self.plt_gui_timer.start()
    
    @Slot()
    def process_new_data(self):
        # print('New data!')
        new_data = self.serial.read()
        self.parser.parse_incoming_data(new_data)

    def print_data(self, data):
        print(f"Unpacked: {data}")

    def enterFullscreenMode(self):
        pass
        # hide toolbar, menu, tab headers

    def leaveFullscreenMode(self):
        pass

    def run_app(self):
        self.main_window.show()
        return self.app.exec()

    def on_app_exit(self):
        print("Exiting Spectrizer...")