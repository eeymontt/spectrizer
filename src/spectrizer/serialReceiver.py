from PySide6.QtSerialPort import QSerialPort
from PySide6.QtCore import QObject, Slot, QIODevice, QByteArray, Signal

class SerialReceiver(QObject):

    connxMade = Signal(str)
    connxLost = Signal()
    readReady = Signal()
    errorOccurred = Signal(str)

    def __init__(self, parent=None):
        super(SerialReceiver, self).__init__()
        self.serial_port = QSerialPort()

        self.serial_port.errorOccurred.connect(self.handle_serial_error)
        self.serial_port.readyRead.connect(lambda: self.readReady.emit())
        self.serial_port.aboutToClose.connect(lambda: print('Serial port is closing...'))

    @Slot(QSerialPort.SerialPortError)
    def handle_serial_error(self, error):
        if error != QSerialPort.NoError:
            # print(error)
            self.serial_port.clearError()
        else:
            error_str = str(error)
            self.errorOccurred.emit(f'Error: {error_str}')

    # def connectRec(self, slotRec):
    #     self.serial_port.readyRead.connect(slotRec)
    
    def open_port(self, portName, baudRate):
        self.serial_port.setPortName(portName)
        print('Attempting open...')
        isOpenSuccess = (
                # self.serial_port.open(QIODevice.ReadWrite)
                self.serial_port.open(QIODevice.ReadOnly)
            and self.serial_port.setBaudRate(baudRate)
            and self.serial_port.setDataBits(QSerialPort.Data8)
            and self.serial_port.setFlowControl(QSerialPort.NoFlowControl)
            and self.serial_port.setParity(QSerialPort.NoParity)
            and self.serial_port.setStopBits(QSerialPort.OneStop))
        self.connxMade.emit('<serial connection name>')
        return isOpenSuccess

    def read(self) -> QByteArray:
        return self.serial_port.readAll()#.data()
        # b = self.serial_port.read(1)
        # print(f"Received: {b.data()}")
        # return b

    def send(self, data):
        return self.serial_port.write(QByteArray(data))

    def close(self):
        self.serial_port.close()
        self.connxLost.emit()