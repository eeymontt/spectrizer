from PySide6.QtCore import (
    QObject,
    QByteArray,
    Signal,
    QRunnable,
    QThreadPool,
    Slot
)

import numpy as np
import struct
import sys
import traceback

# time, alo, ahi, g, m, baro, alt, r/p/y
PACKET_SIZE = 72
binary_format = '<18f'
st = struct.Struct(binary_format)

# bin_fmt_for_testing = '<6f'
# test_struct = struct.Struct(bin_fmt_for_testing)
# TEST_PKT_SIZE = 24
# assert struct.calcsize(bin_fmt_for_testing) == TEST_PKT_SIZE

global cnt
cnt = 0

class DecodeError(Exception):
    '''An exception that covers all sorts of issues with decoding the packets.'''
    pass

def _get_buffer_view(in_bytes) -> memoryview:
    mv = memoryview(in_bytes)
    if mv.ndim > 1 or mv.itemsize > 1:
        raise BufferError('Object must be a single-dimension buffer of bytes.')
    try:
        mv = mv.cast('c')
    except AttributeError:
        pass
    return mv

class PacketParser(QObject):

    newDataAvail = Signal(tuple)
    tooMuchDataError = Signal()
    
    def __init__(self):
        super(PacketParser, self).__init__()
        self.buffer = QByteArray()
        # print(f"buffer is: {self.buffer}")

        self.isAlignedWithPackets = False

    @staticmethod
    def decode(in_bytes: bytes) -> bytes:
        '''
        Decode a string using Consistent Overhead Byte Stuffing (COBS).
        A DecodeError exception will be raised if the encoded data is invalid.
        '''
        if isinstance(in_bytes, str):
            raise TypeError('Unicode-objects are not supported; byte buffer objects only')
        in_bytes_mv: memoryview = _get_buffer_view(in_bytes)
        out_bytes = bytearray()
        idx = 0

        if len(in_bytes_mv) > 0:
            while True:
                length = ord(in_bytes_mv[idx])
                if length == 0:
                    raise DecodeError("Zero byte found in input")
                idx += 1
                end = idx + length - 1
                copy_mv = in_bytes_mv[idx:end]
                if b'\x00' in copy_mv:
                    raise DecodeError("Zero byte found in input")
                out_bytes += copy_mv
                idx = end
                if idx > len(in_bytes_mv):
                    raise DecodeError("Not enough input bytes for length code")
                if idx < len(in_bytes_mv):
                    if length < 0xFF:
                        out_bytes.append(0)
                else:
                    break
        return bytes(out_bytes)

    def parse_incoming_data(self, new_data: QByteArray) -> None:
        global cnt
        cnt += 1

        # @url https://doc.qt.io/qtforpython/PySide6/QtCore/QByteArray.html
        # might need to use of .last(n) and .lastIndexOf(c) if the above is wrong
        if not self.isAlignedWithPackets:
            if new_data.contains(b'\x00'):
                while (not new_data.endsWith(0)):
                    self.buffer.append(new_data.last(1))
                    new_data.chop(1)
                self.isAlignedWithPackets = True
                return
            else:
                return

        self.buffer.append(new_data)
        # print(f"buffer after getting new is: {self.buffer}")

        if self.buffer.contains(b'\x00'):

            packet_bytes: bytes = bytes()
            while self.buffer.count(b'\x00') > 1:
                # count = self.buffer.count(b'\x00')
                # print(f"count: {count}")
                idx = self.buffer.indexOf(b'\x00')
                packet_bytes = self.buffer.first(idx).data()
                self.buffer = self.buffer[idx + 1:]

            # idx = self.buffer.indexOf(b'\x00')
            # packet_bytes = self.buffer.first(idx).data()
            # self.buffer = self.buffer[idx + 1:]
            packet = PacketParser.decode(packet_bytes)
            # print(f"{packet_bytes} -> {packet}")

            try:
                float_data = st.unpack(packet)
            except struct.error as e:
                print(f'Error: {e}')
                return
            
            # print(f'DATA: {float_data}')
            self.newDataAvail.emit(float_data)
            return
        
        else:
            return

        while self.buffer.size() >= PACKET_SIZE:

            ###
            # incoming data is too fast if this is true!
            # it's not premature because we are throttling...
            # @url https://stackoverflow.com/questions/36797088/speed-up-pythons-struct-unpack
            while self.buffer.size() >= 3*PACKET_SIZE:
                self.buffer.chop(PACKET_SIZE)
                self.tooMuchDataError.emit()
                # print('Extra chop!')
            ###

            #data = self.buffer.remove(-PACKET_SIZE, PACKET_SIZE)
            data = self.buffer.first(PACKET_SIZE).data()
            # print(f"buffer after removal is: {self.buffer}")
            
            # print(f"Size of data: {data.size()}")
            float_data = st.unpack(data)
            self.buffer.chop(PACKET_SIZE)
            # print(f"buffer after chop is: {self.buffer}")
            # print(float_data)
            print(f'{cnt}: {float_data}')
            self.newPacket.emit(float_data)