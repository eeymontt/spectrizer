from collections import deque
import numpy as np
import pyqtgraph as pg
from PySide6.QtCore import Qt

class PlotWidget3D(pg.PlotWidget):
    '''Wrapper for working with pg.PlotWidget with 3-DOF data.'''

    CHANNEL_COLORS = ["#F00", "#0F0", "#00F"] # Arguments for pg.mkColor(*args)
    CHANNEL_NAMES = ['X', 'Y', 'Z']

    def __init__(self, parent=None, plotItem=None, bufferSize=100, name=None, **kargs):
        super().__init__(parent=parent, background="w", plotItem=plotItem, **kargs)

        if name == None:
            self.name = 'Untitled'
        else:
            self.name = name

        # Data stuff
        self.curves: list[pg.PlotCurveItem] = []
        for i in range(3):
            curve = pg.PlotCurveItem()
            curve_pen = pg.mkPen(self.CHANNEL_COLORS[i], width=1)
            curve.setPen(curve_pen)
            curve.setSegmentedLineMode('on')
            # curve.setSkipFiniteCheck(True)
            self.curves.append(curve)

        legend = pg.LegendItem(pen=pg.mkPen('#000'), brush=pg.mkBrush('#FFFF'))
        # legend.setBrush(pg.mkBrush('#FFF'))
        # legend.setPen(pg.mkPen('#000'))
        legend.setParentItem(self.plotItem) # called before curves are added
        # legend.setOffset([0,0])
        # legend.setPos(100, 200)
        legend.setOffset([.5, .98])
        # legend.anchor((0,0), (0,0))

        self.view_box = self.plotItem.getViewBox()
        for c, n in zip(self.curves, self.CHANNEL_NAMES):
            self.view_box.addItem(c)
            legend.addItem(c, name=n)

        #self.enableAutoRange(axis='y')
        #self.setAutoVisible(y=True)

        #self.enableAutoRange(axis='x')
        #self.setAutoVisible(x=True)

        ## Data buffers
        self.buffer_size = int(bufferSize)

        # self.x_vals = np.zeros(self.buffer_size, dtype=float)
        # self.x_vals = np.linspace(-self.buffer_size, 0.0, num=self.buffer_size)
        self.x_vals = np.empty(self.buffer_size, dtype=float)
        #self.x_vals[:] = np.nan

        self.data_buffers = []
        self.y = []
        for i in range(3):
            self.data_buffers.append(
                deque([0.0]*self.buffer_size, maxlen=self.buffer_size)
            )
            self.y.append(np.zeros(self.buffer_size, dtype=float))

        # ## Change defaults
        self.styles = {"color": "k", "font-size": "12px"}
        # self.setLabel("left", "Acceleration", **styles)
        # self.setLabel("bottom", u"Time \u00B5s", **self.styles)
        self.setLabel("bottom", u"Time [ms]", **self.styles)
        #self.plotItem.getAxis('bottom').setScale(1e-6)

        # self.setXRange(0, 1, padding=0.02)
        # self.setYRange(0, 5, padding=0.02)

        grid = pg.GridItem()
        grid_pen = pg.mkPen(color=(200, 200, 255), style=Qt.DotLine)
        grid.setPen(grid_pen)
        grid.setTickSpacing(x=[None, None])
        self.addItem(grid)

        self.pen_ch1 = pg.mkPen(color="b", width=1)

    def updateData(self, x, y: list):
        '''Add new data to the plot buffer.'''
        self.x_vals = np.roll(self.x_vals, -1)
        self.x_vals[-1] = x 

        for i in range(len(self.curves)):
            self.data_buffers[i].append(y[i])
            self.y[i][:] = self.data_buffers[i] # move this to updating graphics, maybe... b/c computationally intensive
        
    def updateGraphics(self):
        '''Refresh the plot visual.'''
        for i, curve in enumerate(self.curves):
            # assigned buffered data to the graphics objects
            curve.setData(x=self.x_vals,
                          y=self.y[i])
