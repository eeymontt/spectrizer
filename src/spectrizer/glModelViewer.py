
import OpenGL.GL as GL

import pyqtgraph as pg
import pyqtgraph.opengl as gl
from pyqtgraph.opengl import GLAxisItem, GLGraphicsItem, GLGridItem, GLViewWidget
from pyqtgraph.Qt import QtCore, QtGui

from PySide6.QtGui import QKeyEvent, QMouseEvent
from PySide6 import QtWidgets as qtw
from PySide6 import QtCore as qtc
from PySide6.QtCore import Qt

import numpy as np

class GLPainterItem(GLGraphicsItem.GLGraphicsItem):
    def __init__(self, **kwds):
        super().__init__()
        glopts = kwds.pop('glOptions', 'additive')
        self.setGLOptions(glopts)

    def compute_projection(self):
        modelview = GL.glGetDoublev(GL.GL_MODELVIEW_MATRIX)
        projection = GL.glGetDoublev(GL.GL_PROJECTION_MATRIX)
        mvp = projection.T @ modelview.T
        mvp = QtGui.QMatrix4x4(mvp.ravel().tolist())

        # note that QRectF.bottom() != QRect.bottom()
        rect = QtCore.QRectF(self.view().rect())
        ndc_to_viewport = QtGui.QMatrix4x4()
        ndc_to_viewport.viewport(rect.left(), rect.bottom(), rect.width(), -rect.height())

        return ndc_to_viewport * mvp

    def paint(self):
        self.setupGLState()

        painter = QtGui.QPainter(self.view())
        self.draw(painter)
        painter.end()

    def draw(self, painter):
        painter.setPen(QtCore.Qt.GlobalColor.white)
        painter.setRenderHints(QtGui.QPainter.RenderHint.Antialiasing | QtGui.QPainter.RenderHint.TextAntialiasing)

        rect = self.view().rect()
        af = QtCore.Qt.AlignmentFlag

        painter.drawText(rect, af.AlignTop | af.AlignRight, 'TR')
        painter.drawText(rect, af.AlignBottom | af.AlignLeft, 'BL')
        painter.drawText(rect, af.AlignBottom | af.AlignRight, 'BR')

        opts = self.view().cameraParams()
        lines = []
        center = opts['center']
        lines.append(f"center : ({center.x():.1f}, {center.y():.1f}, {center.z():.1f})")
        for key in ['distance', 'fov', 'elevation', 'azimuth']:
            lines.append(f"{key} : {opts[key]:.1f}")
        xyz = self.view().cameraPosition()
        lines.append(f"xyz : ({xyz.x():.1f}, {xyz.y():.1f}, {xyz.z():.1f})")
        info = "\n".join(lines)
        painter.drawText(rect, af.AlignTop | af.AlignLeft, info)

        project = self.compute_projection()

        hsize = 32 // 2
        for xi in range(-hsize, hsize+1):
            for yi in range(-hsize, hsize+1):
                if xi == -hsize and yi == -hsize:
                    # skip one corner for visual orientation
                    continue
                vec3 = QtGui.QVector3D(xi, yi, 0)
                pos = project.map(vec3).toPointF()
                painter.drawEllipse(pos, 1, 1)

class CubeModelViewer(gl.GLViewWidget):
    def __init__(self, **kwds):
        super().__init__()

        vertexes = np.array([
            [1, 0, 0], #0
            [0, 0, 0], #1
            [0, 1, 0], #2
            [0, 0, 1], #3
            [1, 1, 0], #4
            [1, 1, 1], #5
            [0, 1, 1], #6
            [1, 0, 1], #7
        ])
        faces = np.array([
            [1,0,7], [1,3,7],
            [1,2,4], [1,0,4],
            [1,2,6], [1,3,6],
            [0,4,5], [0,7,5],
            [2,4,5], [2,6,5],
            [3,6,5], [3,7,5],
        ])
        # colors = np.array([[1,0,0,1] for i in range(12)])
        colors = np.array([
            [1, 0, 0, 1],
            [1, 0, 0, 1],
            [0, 1, 0, 1],
            [0, 1, 0, 1],
            [0, 0, 1, 1],
            [0, 0, 1, 1],
            [1, 1, 0, 1],
            [1, 1, 0, 1],
            [0, 1, 1, 1],
            [0, 1, 1, 1],
            [1, 0, 1, 1],
            [1, 0, 1, 1],
        ])

        self.imu_model = gl.GLMeshItem(
            vertexes=vertexes,
            faces=faces,
            faceColors=colors,
            #drawEdges=True,
            #edgeColor=(0, 0, 0, 1),
            smooth=False
        )
        self.imu_model.translate(-.5, -.5, -.5) # move to center
        self.info_viewer = GLPainterItem()

        axes_item = gl.GLAxisItem()
        axes_item.setSize(5, 5, 5)
        self.addItem(axes_item)

        x_axis_text = gl.GLTextItem(pos=[2., 0., 0.], color='#00F', text='X+')
        y_axis_text = gl.GLTextItem(pos=[0., 2., 0.], color='#FF0', text='Y+')
        z_axis_text = gl.GLTextItem(pos=[0., 0., 2.], color='#0F0', text='Z+')
        for t in (x_axis_text, y_axis_text, z_axis_text):
            self.addItem(t)

        x_axis_text = gl.GLTextItem(pos=[-2., 0., 0.], color='#00F', text='X-')
        y_axis_text = gl.GLTextItem(pos=[0., -2., 0.], color='#FF0', text='Y-')
        z_axis_text = gl.GLTextItem(pos=[0., 0., -2.], color='#0F0', text='Z-')
        for t in (x_axis_text, y_axis_text, z_axis_text):
            self.addItem(t)

        self.addItem(self.imu_model)
        self.addItem(self.info_viewer)

        self.setCameraToDefault()

    def setCameraToDefault(self):
        self.setCameraPosition(pos=pg.Vector(0.0, 0.0, 0.0))
        self.setCameraPosition(distance=5, elevation=35, azimuth=-135)
    
    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() == Qt.Key.Key_PageUp:
            opts = self.cameraParams()
            center = opts['center']
            elev = opts['elevation']
            self.setCameraPosition(pos=pg.Vector(center.x(), center.y(), center.z()-.1), elevation=elev-.1)
        elif event.key() == Qt.Key.Key_PageDown:
            opts = self.cameraParams()
            center = opts['center']
            elev = opts['elevation']
            self.setCameraPosition(pos=pg.Vector(center.x(), center.y(), center.z()+.1), elevation=elev+.1)
        elif event.key() == Qt.Key.Key_F:
            event.ignore()
        else:
            return super().keyPressEvent(event)
    
    def mousePressEvent(self, event: QMouseEvent) -> None:
        if event.button() == Qt.MouseButton.RightButton:
            self.setCameraToDefault()
        return super().mousePressEvent(event)

    def setOrientation(self, roll, pitch, yaw):
        self.imu_model.rotate(angle=roll,   x=1, y=0, z=0, local=False)
        self.imu_model.rotate(angle=pitch,  x=0, y=1, z=0, local=False)
        self.imu_model.rotate(angle=yaw,    x=0, y=0, z=1, local=False)

class CubeModelViewerWidget(qtw.QWidget):

    changeFullscreenStatus = qtc.Signal()

    def __init__(self, *args, **kwargs):
        super(CubeModelViewerWidget, self).__init__(*args, **kwargs)

        self.viewer = CubeModelViewer()
        self.vw_layout = qtw.QVBoxLayout()
        self.vw_layout.addWidget(self.viewer)
        self.setLayout(self.vw_layout)

        self.setMinimumSize(250, 200)
        # self._full_screen_widget = None

    def keyPressEvent(self, event: QKeyEvent) -> None:
        if event.key() == Qt.Key.Key_F:
            self.changeFullscreenStatus.emit()
        return super().keyPressEvent(event)