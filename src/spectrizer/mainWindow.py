from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QPushButton,
    QWidget,
    QHBoxLayout,
    QTabWidget,
    QVBoxLayout,
    QSplitter,
    QLabel,
    QDockWidget,
    QSizePolicy
)

from PySide6.QtCore import Qt, QTimer, QObject, Signal, Slot, QIODevice, QByteArray, QSysInfo
import pyqtgraph as pg
import sys
import struct

from spectrizer.graphWidget import PlotWidget3D
from spectrizer.deviceGroupBox import DeviceBox

from spectrizer.glModelViewer import CubeModelViewerWidget

class MainWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi()

    def create_status_bar(self):
        self.normalMsg = QLabel()
        self.normalMsg.setStyleSheet("padding: 2px 2px;")

        self.permenantlMsg = QLabel()
        self.permenantlMsg.setStyleSheet("padding: 2px 2px;")

        # self.normalMsg.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Maximum)
        self.statusBar().addWidget(self.normalMsg, stretch=1)
        self.statusBar().addPermanentWidget(self.permenantlMsg)
        self.normalMsg.setText('Acceleration')
        self.permenantlMsg.setText('Disconnected')

    def create_accelerometer_tab(self):
        self._tab_accel = QWidget()
        self.accel1_layout_widget = PlotWidget3D(name='Acceleration1')
        self.accel2_layout_widget = PlotWidget3D(name='Acceleration2')
        self.accel1_layout_widget.getPlotItem().getViewBox().register('IMU_XYZ')
        self.accel2_layout_widget.getPlotItem().getViewBox().register('ADXL377_XYZ')
        accel_splitter = QSplitter(Qt.Horizontal)
        accel_splitter.addWidget(self.accel1_layout_widget)
        accel_splitter.addWidget(self.accel2_layout_widget)

        # for now...
        self._tab_accel = accel_splitter
    
    def create_gyroscope_tab(self):
        self._tab_gyro = QWidget()
        self.gyro_layout_widget = PlotWidget3D(name='Gyro')
        gyro_vbox_layout = QVBoxLayout()
        gyro_vbox_layout.addWidget(self.gyro_layout_widget)
        self._tab_gyro.setLayout(gyro_vbox_layout)

    def create_magnetometer_tab(self):
        self._tab_mag = QWidget()
        self.mag_layout_widget = PlotWidget3D(name='Mag')
        mag_vbox_layout = QVBoxLayout()
        mag_vbox_layout.addWidget(self.mag_layout_widget)
        self._tab_mag.setLayout(mag_vbox_layout)
    
    def create_orientation_tab(self):
        self._tab_rpy = QWidget()
        self.rpy_layout_widget = PlotWidget3D(name='RPY')
        rpy_vbox_layout = QVBoxLayout()
        rpy_vbox_layout.addWidget(self.rpy_layout_widget)
        self._tab_rpy.setLayout(rpy_vbox_layout)

    def setupUi(self) -> None:
        '''Sets up default user interface.'''
        self.setWindowTitle("Spectrizer")

        self.create_status_bar()

        
        self.create_accelerometer_tab()
        self.create_gyroscope_tab()
        self.create_magnetometer_tab()
        self.create_orientation_tab()

        self.central_tab = QTabWidget()
        self.central_tab.addTab(self._tab_accel, 'Accelerometers')
        self.central_tab.addTab(self._tab_gyro, 'Gyroscope')
        self.central_tab.addTab(self._tab_mag, 'Magnetometer')

        self.central_tab.addTab(QWidget(), 'Atmospheric')
        self.central_tab.addTab(self._tab_rpy, 'Orientation')

        self.central_tab.addTab(QLabel('If you are seeing this, you need to enable some flags.\nSee README.md for details.'), 'Dashboard')

        self.central_tab.addTab(QLabel('This is currently being worked on!'), 'Calibration')

        ## Set up dockable widgets
        
        #  Serial communication interface
        dock1 = QDockWidget('Communication')
        self.device_box = DeviceBox()
        dock1.setWidget(self.device_box)

        #  Viewable 3D model of the module
        dock2 = QDockWidget('3D Model')
        self.glv = CubeModelViewerWidget()
        dock2.setWidget(self.glv)

        for dock in [dock1, dock2]:
            dock.setFeatures(
                QDockWidget.DockWidgetFloatable |
                QDockWidget.DockWidgetMovable |
                QDockWidget.DockWidgetClosable
            )
            dock.setAllowedAreas(Qt.RightDockWidgetArea)
        
        self.addDockWidget(Qt.RightDockWidgetArea, dock1)
        self.addDockWidget(Qt.RightDockWidgetArea, dock2)

        self.plot = PlotWidget3D()
        
        self.content_layout = QHBoxLayout()
        self.content_layout.addWidget(self.central_tab)

        self.setCentralWidget(QWidget())
        self.centralWidget().setLayout(self.content_layout)
        # self.setLayout(self.content_layout)

if __name__ == '__main__':
    #app = QApplication(sys.argv)
    app = QApplication([])
    w = MainWindow()

    # info =  QSysInfo()
    # info_str = f"{info.kernelVersion()}, {info.prettyProductName()}, {info.productVersion()}"
    # print(info_str)

    w.show()
    sys.exit(app.exec())